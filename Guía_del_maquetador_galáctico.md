# Guía del Maquetador Galáctico

## Importante (Work in Progress)

Este documento no es un texto finalizado, es una obra viva en continuo crecimiento, mejora y transformación.

Por ello te aconsejo que, puesto que está alojado en un [repositorio público Git](https://gitlab.com/sergiodiezdepedro/guia-maquetacion-softtek), lo clones y así podrás actualizarlo a la última versión con un simple `git pull`.

Obviamente, está abierto a las sugerencias, críticas o mejoras que consideres oportunas. Dime lo que quieras en sergio.diez@softtek.com, o búscame en Teams como **Sergio Díez de Pedro** si eres un softtekian.

## Introducción

**Don't Panic!!!!**

He incluido algunos puntos interesantes, que tienen importancia cuando te tienes que enfrentar a la maquetación de una página.

No son todos los temas posibles, pero los que menciono merecen una comprensión satisfactoria por su carácter troncal. Créeme.

**Disclaimer**: si, buscando en Google, llegas a https://developer.mozilla.org/ a un contenido en castellano, te aconsejo que cambies a inglés. En ocasiones, las traducciones no son buenas y (casi) siempre son parciales. La documentación completa está en inglés. En esta modesta guía hay enlaces a páginas de esa web en castellano, por lo que, tras revisar lo sugerido te recomiendo que conmutes a inglés.

## ¿Puedo usar esto?

Probablemente ya conozcas este recurso, pero dada su utilidad nunca está de más citarlo.

https://caniuse.com/

## El CSS ya no es lo que era

El CSS sigue creciendo, mejorando, introduciendo nuevas funcionalidades y características. En este apartado se irán incluyendo _features_ recientes que aún no tienen gran soporte en los principales navegadores.

### @scope

¿Qué pasaría si CSS te ofreciera una manera de ser bastante específico sobre qué elementos seleccionas, sin requerir que escribas selectores de alta especificidad o que estén estrechamente acoplados a tu DOM?

Cool, ¿no?

- [MDN Info](https://developer.mozilla.org/en-US/docs/Web/CSS/@scope)

- [Un artículo interesante](https://developer.chrome.com/docs/css-ui/at-scope)

- [Soporte en navegadores](https://caniuse.com/css-cascade-scope)

### CSS Nesting

CSS ya ha dado pasos muy firmes para permitir anidar selectores dentro de otros, tal y como se hace, por ejemplo, en Sass.

[Aprende cómo](https://ishadeed.com/article/css-nesting/).

[En MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_nesting).

[¿Lo puedo usar?](https://caniuse.com/css-nesting).

## Referencia de Elementos HTML

Hay muchos. Más de los que creemos. ¿Menos de los que querríamos?

https://developer.mozilla.org/en-US/docs/Web/HTML/Element

## HTML Semántico

El anfitrión siempre olvidado, pero tan elegante y útil.

![HTML Semántico](img/html-semantico.svg)

https://www.freecodecamp.org/espanol/news/elementos-semanticos-html5-explicados/

[Unos ejemplos](https://dotnettutorials.net/lesson/semantic-elements-html/)

## El modelo de Caja

El origen del Universo.

https://developer.mozilla.org/es/docs/Learn/CSS/Building_blocks/The_box_model

## HTML Block and Inline Elements

No todos somos iguales, ni nos comportamos igual.

https://www.w3schools.com/html/html_blocks.asp

[fit-content](https://gitlab.com/sergiodiezdepedro/fit-content-example) está bien, pero [no altera lo esencial](https://fit-content-example-sergiodiezdepedro-f2f68cfd60b6abbcc5ce2c5cd.gitlab.io/).

## box-sizing: border-box;

El tamaño, a veces, sí importa. Quizás te has preguntado qué diablos es esto y por qué se ve tan a menudo en las hojas de estilo.

https://midu.dev/que-es-y-para-que-sirve-box-sizing-border-box/

Asegúrate de que en tu reset de estilos esté presente, porque te vas a ahorrar un montón de sustos y sobresaltos.

## CSS. Especificidad y herencia

Como le dijo Humpty Dumpty a Alicia, lo importante es saber quién manda.

https://developer.mozilla.org/en-US/docs/Web/CSS/Specificity

https://developer.mozilla.org/es/docs/Learn/CSS/Building_blocks/Cascade_and_inheritance#especificidad

El pez grande se come al chico: https://specifishity.com/

Un calculador online: https://specificity.keegan.st/

Ponte a prueba: https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/Cascade_tasks

## Selectores CSS

Esos (bellos) desconocidos.

https://developer.mozilla.org/es/docs/Web/CSS/CSS_selectors

### Selectores de atributo

https://developer.mozilla.org/en-US/docs/Web/CSS/Attribute_selectors

[Repositorio ejemplo](https://gitlab.com/sergiodiezdepedro/attribute-selectors-example). [Ejemplo online](https://attribute-selectors-example-sergiodiezdepedro-9f9e72b1b5b54a859.gitlab.io/).

## No uses \#IDs en CSS

Cada vez que lo haces [muere un gatito](https://medium.com/@clairecodes/reasons-not-to-use-ids-in-css-a42204fb0d97#:~:text=IDs%20should%20be%20unique%20on,of%20the%20advantages%20of%20CSS.)

## Pseudo-elementos

Detras de esa cosa tan rara, `::`, se esconden agradables sorpresas.

https://developer.mozilla.org/en-US/docs/Web/CSS/Pseudo-elements

## Pseudo-clases

Uno de los caminos por donde más ha progresado el CSS moderno. [Enlace](https://developer.mozilla.org/en-US/docs/Web/CSS/Pseudo-classes)

[:is(), :where() y :has()](https://gitlab.com/sergiodiezdepedro/is-where-has-softtek). Un ejemplo [en acción](https://is-where-has-softtek-sergiodiezdepedro-78d81bdc35a28e23b74a59d0.gitlab.io/).

Selección del [hermano precedente](https://gitlab.com/Frameworks_CSS-Styleguides/preceding-sibling). Un [ejemplo](https://preceding-sibling-frameworks-css-styleguides-ddb824ee5610b6de40.gitlab.io/)

## Flexbox/Grid

... Y dijo el Creador, _hágase la luz_... Y contempló que era bueno...

Ambos estándares son un mundo inacabable, pozos sin fondo llenos de posibilidades.

Empieza por aquí:

- https://css-tricks.com/snippets/css/a-guide-to-flexbox/
- https://css-tricks.com/snippets/css/complete-guide-grid/

Un [ejemplo real](https://gitlab.com/sergiodiezdepedro/grid-templates-areas-softtek).

Grid funciona muy bien con [min-content](https://developer.mozilla.org/en-US/docs/Web/CSS/min-content). [Repositorio](https://gitlab.com/sergiodiezdepedro/min-content-and-grid). [En acción](https://min-content-and-grid-sergiodiezdepedro-59b3b4728dde3d6861aca16a.gitlab.io/)

### ¿Alguien puede explicar de una vez qué demonios significa flex:1?

`flex:` es el _shorthand_ para `flex-grow`, `flex-shrink` y `flex-basis`. Los parámetros segundo y tercero (flex-shrink y flex-basis) son opcionales.

El valor por defecto es **0 1 auto**, pero si se declara un único valor numérico, por ejemplo 1, cambia el valor de flex-basis a 0%, así que es como definir flex-grow: 1; flex-shrink: 1; flex-basis: 0%;.

El [resultado práctico](https://flex-one-sergiodiezdepedro-ddb02a1363708f48f0dc68f542b40b73bab0.gitlab.io/) es que los hijos con esa definición se reparten a partes iguales el ancho del contenedor padre.

## Un Reset moderno

Un ejemplo interesante sobre el que partir y adaptar a nuestras necesidades.

https://www.super-simple.net/blog/un-css-reset-moderno/

## Diseño para desarrolladores

No todo son clases, interceptores, promesas y arrays.

https://developer.mozilla.org/en-US/curriculum/core/design-for-developers/

## Colores

### Colores con valores hexadecimales

¿Qué significan todos esos números y letras? ¿Tiene algún sentido ese galimatías?

https://www.w3schools.com/css/css_colors_hex.asp

La cosa se complica, porque se pueden usar **[8 dígitos](https://www.quackit.com/css/color/values/css_hex_color_notation_8_digits.cfm)**.

Y no pongas las letras del valor hexadecimal en mayúsculas, está feo y el `linting` te va a dar con la regla en los nudillos.

### Colores HSL

¿Qué es eso de [hsl()](https://developer.mozilla.org/en-US/docs/Web/CSS/color_value/hsl)?

Deberías estar usándolo ya en tus proyectos. Si no te fias de mí:

- [Using HSL Colors In CSS](https://www.smashingmagazine.com/2021/07/hsl-colors-css/)
- [Why CSS HSL Colors are Better!](https://elad.medium.com/why-css-hsl-colors-are-better-83b1e0b6eead)
- [Using HSL colors in CSS](https://blog.logrocket.com/using-hsl-colors-css)

### oklch()

La [nueva especificación de color CSS 4](https://www.w3.org/TR/css-color-4/) ha añadido la notación `oklch()` para definir colores. ¿En qué consiste?

`oklch(L C H)` o `oklch(L C H / a)` funciona de este modo:

- **L** es la luminosidad percibida, es decir, la que perciben nuestros ojos. Oscila entre 0% y 100%
- **C** es el chroma, del gris a la tonalidad de color más saturada. Generalmente, de 0 a 0.4/0.5
- **H** es el ángulo de color. De 0 a 360.
- **a** es la opacidad. De 0 a 1 o de 0 a 100%

```css
.lorem-ipsum {
  background: oklch(0.45 0.26 264); /* azul */
  color: oklch(1 0 0); /* blanco */
  border-color: oklch(0 0 0 / 50%); /* negro con un 50% de opacidad */
}
```

A pesar de su reciente introducción, ya tiene un bastante buen [soporte de los navegadores](https://caniuse.com/?search=oklch).

[MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/color_value/oklch).

UN [buen artículo](https://evilmartians.com/chronicles/oklch-in-css-why-quit-rgb-hsl) sobre el tema.

Un [selector/conversor online](https://oklch.com/).

Una paleta de colores generada con oklch(). [Repositorio](https://github.com/sergiodiezdepedro/color-pallets-with-relative-colors). [En acción](https://sergiodiezdepedro.github.io/color-pallets-with-relative-colors/).

## Case Sensitive

[HTML is Not Case Sensitive](https://www.w3schools.com/html/html_elements.asp#:~:text=HTML%20is%20Not%20Case%20Sensitive,stricter%20document%20types%20like%20XHTML.).

HTML tags are not case sensitive:

```html
<p>means the same as</p>
<p></p>
```

The HTML standard does not require lowercase tags, but W3C **recommends** lowercase in HTML, and **demands** lowercase for stricter document types like XHTML.

En CSS es un [asunto algo más complicado](https://alvaromontoro.com/blog/67839/case-insensitive-selectors-in-css).

## El código bien temperado

Se puede presentar código en un archivo HTML de una forma elegante, con la ayuda de los amigos [`<code>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/code) y [`<pre>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/pre).

```html
<pre>
<code>
    <header>
        <h1>Hola Mundo</h1>
    </header>
    <main>
        <section>
            <ul>
                <li>
                    <p>Adiós</p>
                </li>
                <li>
                    <p>Mundo</p>
                </li>
                <li>
                    <p>Cruel</p>
                </li>
            </ul>
        </section>
    </main>
</code>    
</pre>
```

## ¿Y si el contenedor fuera el alcalde?

Gracias a [container-queries](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_containment/Container_queries) se pueden definir estilos en función de las dimensiones del contenedor de un elemento, no del viewport.

Esto permite crear componentes más reutilizables.

[Un ejemplo](https://container-queries-sergiodiezdepedro-1ac2e358a9e6db1e98b8fa9564e.gitlab.io/)

Tranquilos, [se puede usar](https://caniuse.com/?search=container-queries).

## Las (malditas) tablas

Si te piden una tabla _cool_, responsive y elegante [esta puede ser una solución](https://gitlab.com/sergiodiezdepedro/table-responsive-softtek.git)

## Modales con `<dialog>`

- [La teoría](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/dialog)

- [La práctica](https://gitlab.com/sergiodiezdepedro/html-dialog)

- [Ejemplo](https://html-dialog-sergiodiezdepedro-1b68cbc883f8114694218ec7b2065d856.gitlab.io/)

## Los elementos `picture` y `source`

[Repositorio](https://gitlab.com/sergiodiezdepedro/elements-picture-source)

[En acción](https://elements-picture-source-sergiodiezdepedro-52a95b56007a89d6b59ca.gitlab.io/)

## El negociado `<dl>`, `<dt>`, `<dd>`

La etiqueta HTML `<dl>` se utiliza para definir una lista de definiciones (o _description list_ en inglés). Es útil cuando se quiere presentar una lista de términos y sus descripciones o detalles. Este tipo de lista se compone de tres etiquetas principales:

- `<dl>`: para envolver toda la lista de definiciones. [Documentación](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/dl)
- `<dt>`: para definir cada término o nombre que se va a describir. [Documentación](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/dt)
- `<dd>`: para definir la descripción o el detalle del término. [Documentación](https://developer.mozilla.org/es/docs/Web/HTML/Element/dd)

Un ejemplo:

```html
<dl>
  <dt>HTML</dt>
  <dd>Lenguaje de marcado para estructurar contenido en la web.</dd>

  <dt>CSS</dt>
  <dd>Lenguaje de estilo usado para presentar el contenido en la web.</dd>

  <dt>JavaScript</dt>
  <dd>
    Lenguaje de programación que permite crear contenido interactivo en una
    página web.
  </dd>
</dl>
```

Este tipo de lista es útil en situaciones donde el contenido tiene un formato similar al de un diccionario o glosario.

**Casos de uso**

- [Repositorio](https://github.com/sergiodiezdepedro/dt-dl-dd-showcase)
- [En acción](https://sergiodiezdepedro.github.io/dt-dl-dd-showcase/)

**Cuándo utilizar `<dl>`, `<ul>` u `<ol>`**:

- Usa `<dl>` cuando tienes un término y su descripción (como en glosarios, listas de preguntas y respuestas, o listas de términos con explicaciones).
- Para listas de elementos sin un orden específico, usa `<ul>`.
- Para listas ordenadas o numeradas, usa `<ol>`.

## Atributo `data-*`

El atributo HTML `data-*` es un mecanismo que permite almacenar datos personalizados en elementos HTML.

Este tipo de atributos tiene el prefijo `data-` seguido de un nombre que define el tipo de información que se desea almacenar. Por ejemplo, data-id, data-role, data-price, etc.

La sintaxis es flexible y permite agregar varios de estos atributos en un solo elemento.

**Uso y ejemplo**:

El atributo `data-*` se utiliza principalmente para almacenar información que puede ser útil para manipular mediante JavaScript, sin afectar la estructura visual de la página.

```html
<div data-id="1234" data-role="admin" data-price="29.99">Producto ejemplo</div>
```

En este ejemplo:

- `data-id` almacena un identificador único.
- `data-role` define un rol o tipo de usuario.
- `data-price` almacena un precio.

**Acceso desde JavaScript**

JavaScript puede acceder a estos datos usando la propiedad `dataset`:

```javascript
const elemento = document.querySelector("div");

console.log(elemento.dataset.id); // "1234"
console.log(elemento.dataset.role); // "admin"
console.log(elemento.dataset.price); // "29.99"
```

**¿Para qué sirve?**

El atributo `data-*` es útil para:

- Almacenar datos que no se deben mostrar directamente al usuario.
- Añadir información de configuración personalizada.
- Pasar valores específicos para el funcionamiento de scripts.

Es ideal para crear aplicaciones dinámicas en las que se necesita manejar o cambiar valores a nivel de cliente sin sobrecargar el servidor con múltiples solicitudes.

## SASS

### Sass y la línea de comandos

Instala **globalmente** Sass (**Dart Sass**) en tu ordenador como cualquier otro paquete de Node:

```bash
npm install -g sass
```

Puedes compilar Sass a CSS desde la línea de comandos. Y es muy sencillo.

Una vez que Sass esté instalado puedes compilar Sass a CSS usando el comando `sass`. Deberás indicarle desde qué archivo Sass construir y dónde guardar el CSS. Por ejemplo:

```bash
sass input.scss output.css
```

partiría de un único archivo Sass, `input.scss`, y compilaría ese archivo a `output.css`.

También se pueden vigilar archivos individuales o directorios con el _flag_ `--watch`. El flag le dice a Sass que vigile los archivos fuente para detectar cambios, y recompilará el CSS cada vez que guardes un archivo Sass.

Si quisieras vigilar (en lugar de construir manualmente) tu archivo `input.scss`, solo necesitarías añadir el flag así:

```bash
sass --watch input.scss output.css
```

Se pueden observar y enviar resultados a directorios usando rutas de carpetas de entrada y salida, separándolas con dos puntos:

```bash
sass --watch app/sass:public/stylesheets
```

Sass vigilará todos los cambios en los archivos en la carpeta `app/sass` y compilará el CSS a la carpeta `public/stylesheets`.

### El _nuevo_ SASS

Sass ha evolucionado con el tiempo para mantenerse al día. Aquí están algunos de los cambios y características recientes más importantes en Sass:

1. **Sass Module System (Módulo de Sass):**
   Sass introdujo un sistema de módulos que permite organizar y compartir código de manera más efectiva. Ahora puedes importar módulos y controlar mejor el ámbito de las variables y mixins.

2. **Funciones de color avanzadas:**
   con la introducción de funciones de color avanzadas, puedes manipular colores con más precisión. Funciones como `mix()`, `adjust-hue()`, `lighten()`, y `darken()` permiten cambios detallados en los colores.

3. **Sass Maps:**
   los mapas en Sass son una estructura de datos que te permite asociar claves con valores, facilitando la creación de configuraciones flexibles y temas personalizables.

4. **Interpolación mejorada:**
   con el sistema de módulos, la interpolación ha mejorado, permitiendo crear nombres de clases y variables dinámicamente de manera más segura y controlada.

5. **Nuevas funciones y mixins:**
   se han agregado nuevas funciones y mixins para simplificar tareas comunes en el desarrollo de CSS, como operaciones matemáticas avanzadas y creación de estructuras repetitivas.

6. **Compatibilidad con Dart Sass:**
   Dart Sass se ha convertido en la implementación principal de Sass. Proporciona una mejor velocidad y compatibilidad con el sistema de módulos, además de ser el único compilador que se mantiene y actualiza.

7. **Deprecaciones y cambios en la sintaxis:**
   algunos métodos y sintaxis antiguas están siendo deprecadas, como `@import`, en favor de `@use` y `@forward`. Este cambio promueve una estructura de código más modular y evita conflictos de nombres.

En resumen, el nuevo Sass está orientado hacia la modularidad, la reutilización y la flexibilidad.

https://sass-lang.com/documentation/

### @use y algunas nuevas funcionalidades

[Ejemplo](https://gitlab.com/sergiodiezdepedro/new-sass-use)

### Un bucle

Ejemplo de uso de un [bucle en SASS](https://gitlab.com/sergiodiezdepedro/tonos-grises-hsl).

### Una función

```scss
@function rem($elemento, $font-size-base: 16px) {
  $PxToRem: calc($elemento / $font-size-base) * 1rem;
  @return $PxToRem;
}
```

## Selecciona elementos sin JavaScript sin morir en el intento

[Repositorio](https://gitlab.com/sergiodiezdepedro/select-with-nth-of-type)

[Página](https://select-with-nth-of-type-sergiodiezdepedro-e701eb9f42c20e5677e22.gitlab.io/)

## Gradientes modernos

[Repositorio](https://gitlab.com/sergiodiezdepedro/modern-gradients)

[Página](https://modern-gradients-sergiodiezdepedro-68709cb8b933984ad547749876dd.gitlab.io/)

## CSS Modules

Si trabajas habitualmente con JavaScript o con algún framework quizás te interese
[saber qué son y por qué son tan aconsejables](https://css-tricks.com/css-modules-part-1-need/).

[Documentación oficial](https://github.com/css-modules/css-modules)

## Algunos ejemplos prácticos

- [Galería Moderna](https://gitlab.com/sergiodiezdepedro/galeria-moderna)
- [Components Library](https://gitlab.com/sergiodiezdepedro/components-libray) . [Web](https://components-libray-sergiodiezdepedro-882c45eb90c8babcef5f21a5c3b.gitlab.io/).
- Otros usos de `text-wrap`: [Repositorio](https://gitlab.com/sergiodiezdepedro/text-wrap). [En acción](https://text-wrap-sergiodiezdepedro-e2e50d1cd5d6ebb56e6ef0c847650c89db9.gitlab.io/).
- Caso de uso de `subgrid` con filas: [Repositorio](https://gitlab.com/sergiodiezdepedro/subgrid-row-example). [En acción](https://subgrid-row-example-sergiodiezdepedro-b8306b99a2fd65caee1dba8ba.gitlab.io/).
- Otro caso de uso de `subgrid` con tarjetas. [Repositorio](https://github.com/sergiodiezdepedro/subgrid-cards-use-case). [En acción](https://sergiodiezdepedro.github.io/subgrid-cards-use-case/).
